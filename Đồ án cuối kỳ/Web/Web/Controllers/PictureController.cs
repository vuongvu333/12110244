﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;

namespace Web.Controllers
{
    [Authorize]
    public class PictureController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /Picture/
        [AllowAnonymous]
        public ActionResult Index()
        {
            var pictures = db.Pictures.Include(p => p.UserProfile);
            return View(pictures.ToList());
        }

        //
        // GET: /Picture/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            Picture picture = db.Pictures.Find(id);
            ViewData["idpost"] = id;
            if (picture == null)
            {
                return HttpNotFound();
            }
            return View(picture);
        }

        public ActionResult PictureLinkTag(int id = 0)
        {
            Tag tag = db.Tags.Find(id);
            return View(tag);
        }

        public ActionResult PictureLinkCategory(int id = 0)
        {
            Category category = db.Categories.Find(id);
            return View(category);
        }
        //
        // GET: /Picture/Create

        public ActionResult Create(Category ca)
        {
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /Picture/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Picture picture, HttpPostedFileBase file, String TagName, String CategoryName)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    file.SaveAs(HttpContext.Server.MapPath("~/Images/")
                                                          + file.FileName);
                    picture.PictureUrl = file.FileName;
                    picture.UserProfileUserId = db.UserProfiles.Select(s => new { s.UserId, s.UserName })
                    .Where(d => d.UserName == User.Identity.Name).Single().UserId;
                    picture.DateUploaded = DateTime.Now;

                    // tao list cac tag
                    List<Tag> Tags = new List<Tag>();
                    // tach cac tag theo dau ,
                    string[] TagNames = TagName.Split(',');
                    // lap cac tag vua tach
                    foreach (string item in TagNames)
                    {
                        // tim xem tag comtem da co hay chua
                        Tag tagExits = null;
                        var listTag = db.Tags.Where(y => y.TagName.Equals(item));
                        if (listTag.Count() > 0)
                        {
                            // neu co tag roi thi add them post vao
                            tagExits = listTag.First();
                            tagExits.Pictures.Add(picture);

                        }
                        else
                        {
                            // neu chua thi tao tag moi
                            tagExits = new Tag();
                            tagExits.TagName = item;
                            tagExits.Pictures = new List<Picture>();
                            tagExits.Pictures.Add(picture);
                        }
                        //add vao list cac tag
                        Tags.Add(tagExits);
                    }
                    // gan lis tag cho post
                    picture.Tags = Tags;


                    //// tao list cac category
                    //List<Category> Categories = new List<Category>();
                    //// tach cac category theo dau ,
                    //string[] CategoryNames = CategoryName.Split(',');
                    //// lap cac category vua tach
                    //foreach (string item in CategoryNames)
                    //{
                    //    // tim xem category da co hay chua
                    //    Category categoryExits = null;
                    //    var listCategory = db.Categories.Where(y => y.CategoryName.Equals(item));
                    //    if (listCategory.Count() > 0)
                    //    {
                    //        // neu co Categories roi thi add them post vao
                    //        categoryExits = listCategory.First();
                    //        categoryExits.Pictures.Add(picture);

                    //    }
                    //    else
                    //    {
                    //        // neu chua thi tao Categories moi
                    //        categoryExits = new Category();
                    //        categoryExits.CategoryName = item;
                    //        categoryExits.Pictures = new List<Picture>();
                    //        categoryExits.Pictures.Add(picture);
                    //    }
                    //    //add vao list cac Categories
                    //    Categories.Add(categoryExits);
                    //}
                    //// gan lis Categories cho post
                    //picture.Categories = Categories;
                
                }
                
                db.Pictures.Add(picture);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", picture.UserProfileUserId);
            return View(picture);
        }

        //
        // GET: /Picture/Edit/5

        //public ActionResult Edit(int id = 0)
        //{
        //    Picture picture = db.Pictures.Find(id);
        //    if (picture == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", picture.UserProfileUserId);
        //    return View(picture);
        //}

        ////
        //// POST: /Picture/Edit/5

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit(Picture picture)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(picture).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", picture.UserProfileUserId);
        //    return View(picture);
        //}

        //
        // GET: /Picture/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Picture picture = db.Pictures.Find(id);
            if (picture == null)
            {
                return HttpNotFound();
            }
            return View(picture);
        }

        //
        // POST: /Picture/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Picture picture = db.Pictures.Find(id);
            db.Pictures.Remove(picture);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}