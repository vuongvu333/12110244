﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Welcome to Wallpaper4You.com, a community driven wallpaper website!!!";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Contact with me";

            return View();
        }
    }
}
