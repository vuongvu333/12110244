﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;

namespace Web.Controllers
{
    public class ResolutionController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /Resolution/

        public ActionResult Index()
        {
            var resolutions = db.Resolutions.Include(r => r.ResolutionGroup);
            return View(resolutions.ToList());
        }

        //
        // GET: /Resolution/Details/5

        public ActionResult Details(int id = 0)
        {
            Resolution resolution = db.Resolutions.Find(id);
            if (resolution == null)
            {
                return HttpNotFound();
            }
            return View(resolution);
        }

        //
        // GET: /Resolution/Create

        public ActionResult Create()
        {
            ViewBag.ResolutionGroupId = new SelectList(db.ResolutionGroups, "Id", "ResolutionGroupName");
            return View();
        }

        //
        // POST: /Resolution/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Resolution resolution)
        {
            if (ModelState.IsValid)
            {
                db.Resolutions.Add(resolution);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ResolutionGroupId = new SelectList(db.ResolutionGroups, "Id", "ResolutionGroupName", resolution.ResolutionGroupId);
            return View(resolution);
        }

        //
        // GET: /Resolution/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Resolution resolution = db.Resolutions.Find(id);
            if (resolution == null)
            {
                return HttpNotFound();
            }
            ViewBag.ResolutionGroupId = new SelectList(db.ResolutionGroups, "Id", "ResolutionGroupName", resolution.ResolutionGroupId);
            return View(resolution);
        }

        //
        // POST: /Resolution/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Resolution resolution)
        {
            if (ModelState.IsValid)
            {
                db.Entry(resolution).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ResolutionGroupId = new SelectList(db.ResolutionGroups, "Id", "ResolutionGroupName", resolution.ResolutionGroupId);
            return View(resolution);
        }

        //
        // GET: /Resolution/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Resolution resolution = db.Resolutions.Find(id);
            if (resolution == null)
            {
                return HttpNotFound();
            }
            return View(resolution);
        }

        //
        // POST: /Resolution/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Resolution resolution = db.Resolutions.Find(id);
            db.Resolutions.Remove(resolution);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}