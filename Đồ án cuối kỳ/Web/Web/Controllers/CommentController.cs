﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;

namespace Web.Controllers
{
    public class CommentController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /Comment/

        public ActionResult Index()
        {
            var comments = db.Comments.Include(c => c.Picture).Include(c => c.UserProfile);
            return View(comments.ToList());
        }

        //
        // GET: /Comment/Details/5

        public ActionResult Details(int id = 0)
        {
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(comment);
        }

        //
        // GET: /Comment/Create

        public ActionResult Create()
        {
            ViewBag.PictureId = new SelectList(db.Pictures, "Id", "PictureName");
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /Comment/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Comment comment, int idpost)
        {
            if (ModelState.IsValid)
            {
                comment.DateCreated = DateTime.Now;
                comment.PictureId = idpost;
                comment.UserProfileUserId = db.UserProfiles.Select(s => new { s.UserId, s.UserName })
                    .Where(d => d.UserName == User.Identity.Name).Single().UserId;
                db.Comments.Add(comment);
                db.SaveChanges();
                return PartialView("viewComment", comment);
                //return RedirectToAction("Index");
            }

            ViewBag.PictureId = new SelectList(db.Pictures, "Id", "PictureName", comment.PictureId);
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", comment.UserProfileUserId);
            return View(comment);
        }

        //
        // GET: /Comment/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            ViewBag.PictureId = new SelectList(db.Pictures, "Id", "PictureName", comment.PictureId);
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", comment.UserProfileUserId);
            return View(comment);
        }

        //
        // POST: /Comment/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Comment comment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(comment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PictureId = new SelectList(db.Pictures, "Id", "PictureName", comment.PictureId);
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", comment.UserProfileUserId);
            return View(comment);
        }

        //
        // GET: /Comment/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(comment);
        }

        //
        // POST: /Comment/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Comment comment = db.Comments.Find(id);
            db.Comments.Remove(comment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}