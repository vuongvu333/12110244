﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;

namespace Web.Controllers
{
    public class ArchievementController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /Archievement/

        public ActionResult Index()
        {
            var archievements = db.Archievements.Include(a => a.ArchievementGroup);
            return View(archievements.ToList());
        }

        //
        // GET: /Archievement/Details/5

        public ActionResult Details(int id = 0)
        {
            Archievement archievement = db.Archievements.Find(id);
            if (archievement == null)
            {
                return HttpNotFound();
            }
            return View(archievement);
        }

        //
        // GET: /Archievement/Create

        public ActionResult Create()
        {
            ViewBag.ArchievementGroupId = new SelectList(db.ArchievementGroups, "Id", "ArchievementGroupName");
            return View();
        }

        //
        // POST: /Archievement/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Archievement archievement)
        {
            if (ModelState.IsValid)
            {
                db.Archievements.Add(archievement);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ArchievementGroupId = new SelectList(db.ArchievementGroups, "Id", "ArchievementGroupName", archievement.ArchievementGroupId);
            return View(archievement);
        }

        //
        // GET: /Archievement/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Archievement archievement = db.Archievements.Find(id);
            if (archievement == null)
            {
                return HttpNotFound();
            }
            ViewBag.ArchievementGroupId = new SelectList(db.ArchievementGroups, "Id", "ArchievementGroupName", archievement.ArchievementGroupId);
            return View(archievement);
        }

        //
        // POST: /Archievement/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Archievement archievement)
        {
            if (ModelState.IsValid)
            {
                db.Entry(archievement).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ArchievementGroupId = new SelectList(db.ArchievementGroups, "Id", "ArchievementGroupName", archievement.ArchievementGroupId);
            return View(archievement);
        }

        //
        // GET: /Archievement/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Archievement archievement = db.Archievements.Find(id);
            if (archievement == null)
            {
                return HttpNotFound();
            }
            return View(archievement);
        }

        //
        // POST: /Archievement/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Archievement archievement = db.Archievements.Find(id);
            db.Archievements.Remove(archievement);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}