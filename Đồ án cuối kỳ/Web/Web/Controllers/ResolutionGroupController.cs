﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;

namespace Web.Controllers
{
    public class ResolutionGroupController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /ResolutionGroup/

        public ActionResult Index()
        {
            return View(db.ResolutionGroups.ToList());
        }

        //
        // GET: /ResolutionGroup/Details/5

        public ActionResult Details(int id = 0)
        {
            ResolutionGroup resolutiongroup = db.ResolutionGroups.Find(id);
            if (resolutiongroup == null)
            {
                return HttpNotFound();
            }
            return View(resolutiongroup);
        }

        //
        // GET: /ResolutionGroup/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /ResolutionGroup/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ResolutionGroup resolutiongroup)
        {
            if (ModelState.IsValid)
            {
                db.ResolutionGroups.Add(resolutiongroup);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(resolutiongroup);
        }

        //
        // GET: /ResolutionGroup/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ResolutionGroup resolutiongroup = db.ResolutionGroups.Find(id);
            if (resolutiongroup == null)
            {
                return HttpNotFound();
            }
            return View(resolutiongroup);
        }

        //
        // POST: /ResolutionGroup/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ResolutionGroup resolutiongroup)
        {
            if (ModelState.IsValid)
            {
                db.Entry(resolutiongroup).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(resolutiongroup);
        }

        //
        // GET: /ResolutionGroup/Delete/5

        public ActionResult Delete(int id = 0)
        {
            ResolutionGroup resolutiongroup = db.ResolutionGroups.Find(id);
            if (resolutiongroup == null)
            {
                return HttpNotFound();
            }
            return View(resolutiongroup);
        }

        //
        // POST: /ResolutionGroup/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ResolutionGroup resolutiongroup = db.ResolutionGroups.Find(id);
            db.ResolutionGroups.Remove(resolutiongroup);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}