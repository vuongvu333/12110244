﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class ResolutionGroup
    {
        //nhóm kích thước ảnh
        public int Id { get; set; }
        public String ResolutionGroupName { get; set; }

        public virtual ICollection<Resolution> Resolutions { get; set; }
    }
}