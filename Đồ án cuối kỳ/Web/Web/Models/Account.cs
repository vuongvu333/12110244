﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;

namespace Web.Models
{
    public class WebDbContext : DbContext
    {
        public WebDbContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Archievement> Archievements { get; set; }
        public DbSet<ArchievementGroup> ArchievementGroups { get; set; }
        public DbSet<Picture> Pictures { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<CategoryGroup> CategoryGroups { get; set; }
        public DbSet<ResolutionGroup> ResolutionGroups { get; set; }
        public DbSet<Resolution> Resolutions { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Rate> Rates { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<UserProfile>()
                .HasMany(a => a.Archievements).WithMany(s => s.UserProfiles)
                .Map(d =>
                {
                    d.MapLeftKey("UserID");
                    d.MapRightKey("ArchievementID");
                    d.ToTable("User_Archievement");
                });
            modelBuilder.Entity<Picture>()
                .HasMany(a => a.Tags).WithMany(s => s.Pictures)
                .Map(d =>
                {
                    d.MapLeftKey("PictureID");
                    d.MapRightKey("TagID");
                    d.ToTable("Picture_Tag");
                });
            modelBuilder.Entity<Picture>()
                .HasMany(a => a.Categories).WithMany(s => s.Pictures)
                .Map(d =>
                {
                    d.MapLeftKey("PictureID");
                    d.MapRightKey("CategoryID");
                    d.ToTable("Picture_Category");
                });
            modelBuilder.Entity<Picture>()
                .HasMany(a => a.Resolutions).WithMany(s => s.Pictures)
                .Map(d =>
                {
                    d.ToTable("Picture_Resolution");
                    d.MapLeftKey("PictureID");
                    d.MapRightKey("ResolutionID");
                });
        }
    }

        [Table("UserProfile")]
        public class UserProfile
        {
            [Key]
            [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
            public int UserId { get; set; }
            public string UserName { get; set; }
            public string Email { get; set; }
            public DateTime Date_Registration { get; set; } //ngày đăng ký
            public String Avartar { get; set; } // ảnh đại diện
            public String Group { get; set; } // cấp bậc người dùng - ex:newbie
            public int Ranking { get; set; } //Thứ hạng người dùng
            public int Archievements_Gained { get; set; }//sô huy chương - huy hiệu đạt được
            public int Points { get; set; } // Tổng điểm của người dùng
            public int Profile_views { get; set; } // lượt xem trang cá nhân người dùng 
            public int Messages_sents { get; set; }// tin nhắn đã gửi
            public int Upload_views { get; set; }// lượt xem hình upload của người dùng
            public int Upload_downloads { get; set; }//lượt download hình của người dùng
            public int Upload_favourites { get; set; }// lượt đánh giá yêu thích ảnh của người dùng
            public double Rating { get; set; } // điểm đánh giá ảnh (toàn bộ ảnh) của người dùng
            public int Uploaded_wallpapers { get; set; } // số ảnh đã đăng
            public int Favourites { get; set; } //sô ảnh người dùng yêu thích
            public int Category_editings { get; set; }
            public int Tags_editings { get; set; }
            public int Marksfordeletion { get; set; }

            public virtual ICollection<Archievement> Archievements { get; set; }
            public virtual ICollection<Picture> Pictures { get; set; }
            public virtual ICollection<Comment> Comments { get; set; }
        }

        public class LostPasswordModel
        {
            [Required(ErrorMessage = "We need your email to send you a reset link!")]
            [Display(Name = "Your account email")]
            [EmailAddress(ErrorMessage = "Not a valid email--what are you trying to do here?")]
            public string Email { get; set; }
        }

        public class ResetPasswordModel
        {
            [Required]
            [Display(Name = "New Password")]
            [DataType(DataType.Password)]
            public string Password { get; set; }

            [Required]
            [Display(Name = "Confirm Password")]
            [DataType(DataType.Password)]
            [Compare("Password", ErrorMessage = "New password and confirmation does not match.")]
            public string ConfirmPassword { get; set; }

            [Required]
            public string ReturnToken { get; set; }
        }

        public class RegisterExternalLoginModel
        {
            [Required]
            [Display(Name = "User name")]
            public string UserName { get; set; }

            public string ExternalLoginData { get; set; }
        }

        public class LocalPasswordModel
        {
            [Required]
            [DataType(DataType.Password)]
            [Display(Name = "Current password")]
            public string OldPassword { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "New password")]
            public string NewPassword { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm new password")]
            [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }
        }

        public class LoginModel
        {
            [Required]
            [Display(Name = "User name")]
            public string UserName { get; set; }

            [Required]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [Display(Name = "Remember me?")]
            public bool RememberMe { get; set; }
        }

        public class RegisterModel
        {
            [Required]
            [Display(Name = "User name")]
            public string UserName { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }

            [Required]
            [RegularExpression("\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*", ErrorMessage = "Please enter a valid email address.")]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }
        }

        public class ExternalLogin
        {
            public string Provider { get; set; }
            public string ProviderDisplayName { get; set; }
            public string ProviderUserId { get; set; }
        }
    }
