﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class Picture
    {
        public int Id { get; set; }
        public String PictureName { get; set; }
        [DataType(DataType.ImageUrl)]
        public string PictureUrl { get; set; }
        public int Favourites { get; set; } //số lượt thích bức ảnh
        public int Downloads { get; set; } // số lượt download bức ảnh
        public double TotalRates { get; set; }// điểm tổng đánh giá ảnh
        public DateTime DateUploaded { get; set; }

        public int UserProfileUserId { get; set; }
        public virtual UserProfile UserProfile { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
        public virtual ICollection<Category> Categories { get; set; }
        public virtual ICollection<Resolution> Resolutions { get; set; }
        public virtual ICollection<Rate> Rates { get; set; }
    }
}