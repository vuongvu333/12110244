﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class CategoryGroup
    {
        //nhóm danh mục ảnh
        public int Id { get; set; }
        public String CategoryGroupName { get; set; }

        public virtual ICollection<Category> Categories { get; set; }
    }
}