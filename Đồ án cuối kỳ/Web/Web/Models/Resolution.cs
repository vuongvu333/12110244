﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class Resolution
    {
        //Kích thước riêng
        public int ResolutionId { get; set; }
        public String ResolutionName { get; set; }

        public int ResolutionGroupId { get; set; }
        public virtual ResolutionGroup ResolutionGroup { get; set; }
        public virtual ICollection<Picture> Pictures { get; set; }
    }
}