﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class Archievement
    {
        //Tên huy chương -  huy hiệu
        public int Id { get; set; }
        public String ArchievementName { get; set; }
        public int Point { get; set; }// điểm của huy chương - huy hiệu

        public int ArchievementGroupId { get; set; }
        public virtual ArchievementGroup ArchievementGroup { get; set; }
        public virtual ICollection<UserProfile> UserProfiles { get; set; }
    }
}