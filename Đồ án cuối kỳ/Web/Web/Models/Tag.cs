﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class Tag
    {
        public int TagId { get; set; }
        public String TagName { get; set; }

        public virtual ICollection<Picture> Pictures { get; set; }
    }
}