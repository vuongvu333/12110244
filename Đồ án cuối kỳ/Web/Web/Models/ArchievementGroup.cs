﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class ArchievementGroup
    {
        //Nhóm huy chương -  huy hiệu
        public int Id { get; set; }
        public String ArchievementGroupName { get; set; }

        public virtual ICollection<Archievement> Archievements { get; set; }
    }
}