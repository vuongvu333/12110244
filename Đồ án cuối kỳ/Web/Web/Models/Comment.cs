﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class Comment
    {
        public int Id { get; set; }
        public String Content { get; set;}
        public DateTime DateCreated { get; set; }

        public int PictureId { get; set; }
        public virtual Picture Picture { get; set; }
        public int UserProfileUserId { get; set; }
        public virtual UserProfile UserProfile { get; set; }
    }
}