﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class Rate
    {
        [Key]
        [Column(Order=0)]
        public int PictureId { get; set; }
        [Key]
        [Column(Order = 1)]
        public int UserProfileUserId { get; set; }
        public int RatingValue { get; set; }

        public virtual Picture Picture { get; set; }
        public virtual UserProfile UserProfile { get; set; }
    }
}