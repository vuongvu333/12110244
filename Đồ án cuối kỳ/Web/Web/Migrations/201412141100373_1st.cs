namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _1st : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                        Email = c.String(),
                        Date_Registration = c.DateTime(nullable: false),
                        Avartar = c.String(),
                        Group = c.String(),
                        Ranking = c.Int(nullable: false),
                        Archievements_Gained = c.Int(nullable: false),
                        Points = c.Int(nullable: false),
                        Profile_views = c.Int(nullable: false),
                        Messages_sents = c.Int(nullable: false),
                        Upload_views = c.Int(nullable: false),
                        Upload_downloads = c.Int(nullable: false),
                        Upload_favourites = c.Int(nullable: false),
                        Rating = c.Double(nullable: false),
                        Uploaded_wallpapers = c.Int(nullable: false),
                        Favourites = c.Int(nullable: false),
                        Category_editings = c.Int(nullable: false),
                        Tags_editings = c.Int(nullable: false),
                        Marksfordeletion = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Archievements",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ArchievementName = c.String(),
                        Point = c.Int(nullable: false),
                        ArchievementGroupId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ArchievementGroups", t => t.ArchievementGroupId, cascadeDelete: true)
                .Index(t => t.ArchievementGroupId);
            
            CreateTable(
                "dbo.ArchievementGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ArchievementGroupName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Pictures",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PictureName = c.String(),
                        PictureUrl = c.String(),
                        Favourites = c.Int(nullable: false),
                        Downloads = c.Int(nullable: false),
                        TotalRates = c.Double(nullable: false),
                        DateUploaded = c.DateTime(nullable: false),
                        UserProfileUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId, cascadeDelete: true)
                .Index(t => t.UserProfileUserId);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Content = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        Author = c.String(),
                        PictureId = c.Int(nullable: false),
                        UserProfileUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Pictures", t => t.PictureId, cascadeDelete: true)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId, cascadeDelete: false)
                .Index(t => t.PictureId)
                .Index(t => t.UserProfileUserId);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        TagId = c.Int(nullable: false, identity: true),
                        TagName = c.String(),
                    })
                .PrimaryKey(t => t.TagId);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        CategoryId = c.Int(nullable: false, identity: true),
                        CategoryName = c.String(),
                        CategoryGroupId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CategoryId)
                .ForeignKey("dbo.CategoryGroups", t => t.CategoryGroupId, cascadeDelete: true)
                .Index(t => t.CategoryGroupId);
            
            CreateTable(
                "dbo.CategoryGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CategoryGroupName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Resolutions",
                c => new
                    {
                        ResolutionId = c.Int(nullable: false, identity: true),
                        ResolutionName = c.String(),
                        ResolutionGroupId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ResolutionId)
                .ForeignKey("dbo.ResolutionGroups", t => t.ResolutionGroupId, cascadeDelete: true)
                .Index(t => t.ResolutionGroupId);
            
            CreateTable(
                "dbo.ResolutionGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ResolutionGroupName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Rates",
                c => new
                    {
                        PictureId = c.Int(nullable: false),
                        UserProfileUserId = c.Int(nullable: false),
                        RatingValue = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PictureId, t.UserProfileUserId })
                .ForeignKey("dbo.Pictures", t => t.PictureId, cascadeDelete: true)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId, cascadeDelete: false)
                .Index(t => t.PictureId)
                .Index(t => t.UserProfileUserId);
            
            CreateTable(
                "dbo.User_Archievement",
                c => new
                    {
                        UserID = c.Int(nullable: false),
                        ArchievementID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserID, t.ArchievementID })
                .ForeignKey("dbo.UserProfile", t => t.UserID, cascadeDelete: true)
                .ForeignKey("dbo.Archievements", t => t.ArchievementID, cascadeDelete: true)
                .Index(t => t.UserID)
                .Index(t => t.ArchievementID);
            
            CreateTable(
                "dbo.Picture_Tag",
                c => new
                    {
                        PictureID = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PictureID, t.TagID })
                .ForeignKey("dbo.Pictures", t => t.PictureID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.PictureID)
                .Index(t => t.TagID);
            
            CreateTable(
                "dbo.Picture_Category",
                c => new
                    {
                        PictureID = c.Int(nullable: false),
                        CategoryID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PictureID, t.CategoryID })
                .ForeignKey("dbo.Pictures", t => t.PictureID, cascadeDelete: true)
                .ForeignKey("dbo.Categories", t => t.CategoryID, cascadeDelete: true)
                .Index(t => t.PictureID)
                .Index(t => t.CategoryID);
            
            CreateTable(
                "dbo.Picture_Resolution",
                c => new
                    {
                        PictureID = c.Int(nullable: false),
                        ResolutionID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PictureID, t.ResolutionID })
                .ForeignKey("dbo.Pictures", t => t.PictureID, cascadeDelete: true)
                .ForeignKey("dbo.Resolutions", t => t.ResolutionID, cascadeDelete: true)
                .Index(t => t.PictureID)
                .Index(t => t.ResolutionID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Picture_Resolution", new[] { "ResolutionID" });
            DropIndex("dbo.Picture_Resolution", new[] { "PictureID" });
            DropIndex("dbo.Picture_Category", new[] { "CategoryID" });
            DropIndex("dbo.Picture_Category", new[] { "PictureID" });
            DropIndex("dbo.Picture_Tag", new[] { "TagID" });
            DropIndex("dbo.Picture_Tag", new[] { "PictureID" });
            DropIndex("dbo.User_Archievement", new[] { "ArchievementID" });
            DropIndex("dbo.User_Archievement", new[] { "UserID" });
            DropIndex("dbo.Rates", new[] { "UserProfileUserId" });
            DropIndex("dbo.Rates", new[] { "PictureId" });
            DropIndex("dbo.Resolutions", new[] { "ResolutionGroupId" });
            DropIndex("dbo.Categories", new[] { "CategoryGroupId" });
            DropIndex("dbo.Comments", new[] { "UserProfileUserId" });
            DropIndex("dbo.Comments", new[] { "PictureId" });
            DropIndex("dbo.Pictures", new[] { "UserProfileUserId" });
            DropIndex("dbo.Archievements", new[] { "ArchievementGroupId" });
            DropForeignKey("dbo.Picture_Resolution", "ResolutionID", "dbo.Resolutions");
            DropForeignKey("dbo.Picture_Resolution", "PictureID", "dbo.Pictures");
            DropForeignKey("dbo.Picture_Category", "CategoryID", "dbo.Categories");
            DropForeignKey("dbo.Picture_Category", "PictureID", "dbo.Pictures");
            DropForeignKey("dbo.Picture_Tag", "TagID", "dbo.Tags");
            DropForeignKey("dbo.Picture_Tag", "PictureID", "dbo.Pictures");
            DropForeignKey("dbo.User_Archievement", "ArchievementID", "dbo.Archievements");
            DropForeignKey("dbo.User_Archievement", "UserID", "dbo.UserProfile");
            DropForeignKey("dbo.Rates", "UserProfileUserId", "dbo.UserProfile");
            DropForeignKey("dbo.Rates", "PictureId", "dbo.Pictures");
            DropForeignKey("dbo.Resolutions", "ResolutionGroupId", "dbo.ResolutionGroups");
            DropForeignKey("dbo.Categories", "CategoryGroupId", "dbo.CategoryGroups");
            DropForeignKey("dbo.Comments", "UserProfileUserId", "dbo.UserProfile");
            DropForeignKey("dbo.Comments", "PictureId", "dbo.Pictures");
            DropForeignKey("dbo.Pictures", "UserProfileUserId", "dbo.UserProfile");
            DropForeignKey("dbo.Archievements", "ArchievementGroupId", "dbo.ArchievementGroups");
            DropTable("dbo.Picture_Resolution");
            DropTable("dbo.Picture_Category");
            DropTable("dbo.Picture_Tag");
            DropTable("dbo.User_Archievement");
            DropTable("dbo.Rates");
            DropTable("dbo.ResolutionGroups");
            DropTable("dbo.Resolutions");
            DropTable("dbo.CategoryGroups");
            DropTable("dbo.Categories");
            DropTable("dbo.Tags");
            DropTable("dbo.Comments");
            DropTable("dbo.Pictures");
            DropTable("dbo.ArchievementGroups");
            DropTable("dbo.Archievements");
            DropTable("dbo.UserProfile");
        }
    }
}
