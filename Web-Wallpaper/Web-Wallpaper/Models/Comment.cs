﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_Wallpaper.Models
{
    public class Comment
    {
        public int ID { get; set; }
        public String Content { get; set;}
        public DateTime DateCreated { get; set; }
        public String Author { get; set; }

        public int PictureID { get; set; }
        public virtual Picture Picture { get; set; }
    }
}