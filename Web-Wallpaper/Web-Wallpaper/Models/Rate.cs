﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_Wallpaper.Models
{
    public class Rate
    {
        public int ID { get; set; }
        public int PictureID { get; set; }

        public virtual Picture Picture { get; set; }
    }
}