﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_Wallpaper.Models
{
    public class Resolution
    {
        //Kích thước riêng
        public int ResolutionID { get; set; }
        public String ResolutionName { get; set; }

        public int ResolutionGroupID { get; set; }
        public virtual ResolutionGroup ResolutionGroup { get; set; }
        public virtual ICollection<Picture> Pictures { get; set; }
    }
}