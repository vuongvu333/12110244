﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_Wallpaper.Models
{
    public class Tag
    {
        public int TagID { get; set; }
        public String TagName { get; set; }

        public virtual ICollection<Picture> Pictures { get; set; }
    }
}