﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_Wallpaper.Models
{
    public class Archievement
    {
        //Tên huy chương -  huy hiệu
        public int ID { get; set; }
        public String ArchievementName { get; set; }
        public int Point { get; set; }// điểm của huy chương - huy hiệu

        public int ArchievementGroupID { get; set; }
        public virtual ArchievementGroup ArchievementGroup { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}