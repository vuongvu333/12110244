﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_Wallpaper.Models
{
    public class User
    {
        public int UserID { get; set; }
        public String Username { get; set; }
        public String Password { get; set; }
        public String Email { get; set; }
        public DateTime Date_Registration { get; set; } //ngày đăng ký
        public String Avartar { get; set; } // ảnh đại diện
        public String Group { get; set; } // cấp bậc người dùng - ex:newbie
        public int Ranking { get; set; } //Thứ hạng người dùng
        public int Archievements_Gained { get; set; }//sô huy chương - huy hiệu đạt được
        public int Points { get; set; } // Tổng điểm của người dùng
        public int Profile_views { get; set; } // lượt xem trang cá nhân người dùng 
        public int Messages_sents { get; set; }// tin nhắn đã gửi
        public int Upload_views { get; set; }// lượt xem hình upload của người dùng
        public int Upload_downloads { get; set; }//lượt download hình của người dùng
        public int Upload_favourites { get; set; }// lượt đánh giá yêu thích ảnh của người dùng
        public double Rating { get; set; } // điểm đánh giá ảnh (toàn bộ ảnh) của người dùng
        public int Uploaded_wallpapers { get; set; } // số ảnh đã đăng
        public int Favourites { get; set; } //sô ảnh người dùng yêu thích
        public int Category_editings { get; set; }
        public int Tags_editings { get; set; }
        public int Marksfordeletion { get; set; }

        public virtual ICollection<Archievement> Archievements { get; set; }
        public virtual ICollection<Picture> Pictures { get; set; }
    }
}