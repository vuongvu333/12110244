﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Web_Wallpaper.Models
{
    public class WebDbContext: DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Archievement> Archievements { get; set; }
        public DbSet<ArchievementGroup> ArchievementGroups { get; set; }
        public DbSet<Picture> Pictures { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<CategoryGroup> CategoryGroups { get; set; }
        public DbSet<ResolutionGroup> ResolutionGroups { get; set; }
        public DbSet<Resolution> Resolutions { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Rate> Rates { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<User>()
                .HasMany(a => a.Archievements).WithMany(s => s.Users)
                .Map(d =>
                {
                    d.MapLeftKey("UserID");
                    d.MapRightKey("ArchievementID");
                    d.ToTable("User_Archievement");
                });
            modelBuilder.Entity<Picture>()
                .HasMany(a => a.Tags).WithMany(s => s.Pictures)
                .Map(d =>
                {
                    d.MapLeftKey("PictureID");
                    d.MapRightKey("TagID");
                    d.ToTable("Picture_Tag");
                });
            modelBuilder.Entity<Picture>()
                .HasMany(a => a.Categories).WithMany(s => s.Pictures)
                .Map(d =>
                {
                    d.MapLeftKey("PictureID");
                    d.MapRightKey("CategoryID");
                    d.ToTable("Picture_Category");
                });
            modelBuilder.Entity<Picture>()
                .HasMany(a => a.Resolutions).WithMany(s => s.Pictures)
                .Map(d =>
                {
                    d.ToTable("Picture_Resolution");
                    d.MapLeftKey("PictureID");
                    d.MapRightKey("ResolutionID");
                });
        }
    }
}