﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_Wallpaper.Models
{
    public class Category
    {
        //Danh mục riêng
        public int CategoryID { get; set; }
        public String CategoryName { get; set; }

        public int CategoryGroupID { get; set; }
        public virtual CategoryGroup CategoryGroup { get; set; }
        public virtual ICollection<Picture> Pictures { get; set; }
    }
}