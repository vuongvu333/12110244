namespace Web_Wallpaper.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _1st : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserID = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        Password = c.String(),
                        Email = c.String(),
                        Date_Registration = c.DateTime(nullable: false),
                        Avartar = c.String(),
                        Group = c.String(),
                        Ranking = c.Int(nullable: false),
                        Archievements_Gained = c.Int(nullable: false),
                        Points = c.Int(nullable: false),
                        Profile_views = c.Int(nullable: false),
                        Messages_sents = c.Int(nullable: false),
                        Upload_views = c.Int(nullable: false),
                        Upload_downloads = c.Int(nullable: false),
                        Upload_favourites = c.Int(nullable: false),
                        Rating = c.Double(nullable: false),
                        Uploaded_wallpapers = c.Int(nullable: false),
                        Favourites = c.Int(nullable: false),
                        Category_editings = c.Int(nullable: false),
                        Tags_editings = c.Int(nullable: false),
                        Marksfordeletion = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserID);
            
            CreateTable(
                "dbo.Archievements",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Point = c.Int(nullable: false),
                        ArchievementGroupID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ArchievementGroups", t => t.ArchievementGroupID, cascadeDelete: true)
                .Index(t => t.ArchievementGroupID);
            
            CreateTable(
                "dbo.ArchievementGroups",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Pictures",
                c => new
                    {
                        PictureID = c.Int(nullable: false, identity: true),
                        PictureName = c.String(),
                        Favourites = c.Int(nullable: false),
                        Downloads = c.Int(nullable: false),
                        TotalRates = c.Double(nullable: false),
                        DateUploaded = c.DateTime(nullable: false),
                        UserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PictureID)
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Content = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        Author = c.String(),
                        PictureID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Pictures", t => t.PictureID, cascadeDelete: true)
                .Index(t => t.PictureID);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        TagID = c.Int(nullable: false, identity: true),
                        TagName = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TagID);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        CategoryID = c.Int(nullable: false, identity: true),
                        CategoryName = c.String(),
                        CategoryGroupID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CategoryID)
                .ForeignKey("dbo.CategoryGroups", t => t.CategoryGroupID, cascadeDelete: true)
                .Index(t => t.CategoryGroupID);
            
            CreateTable(
                "dbo.CategoryGroups",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Resolutions",
                c => new
                    {
                        ResolutionID = c.Int(nullable: false, identity: true),
                        ResolutionName = c.String(),
                        ResolutionGroupID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ResolutionID)
                .ForeignKey("dbo.ResolutionGroups", t => t.ResolutionGroupID, cascadeDelete: true)
                .Index(t => t.ResolutionGroupID);
            
            CreateTable(
                "dbo.ResolutionGroups",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Rates",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PictureID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Pictures", t => t.PictureID, cascadeDelete: true)
                .Index(t => t.PictureID);
            
            CreateTable(
                "dbo.User_Archievement",
                c => new
                    {
                        UserID = c.Int(nullable: false),
                        ArchievementID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserID, t.ArchievementID })
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: true)
                .ForeignKey("dbo.Archievements", t => t.ArchievementID, cascadeDelete: true)
                .Index(t => t.UserID)
                .Index(t => t.ArchievementID);
            
            CreateTable(
                "dbo.Picture_Tag",
                c => new
                    {
                        PictureID = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PictureID, t.TagID })
                .ForeignKey("dbo.Pictures", t => t.PictureID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.PictureID)
                .Index(t => t.TagID);
            
            CreateTable(
                "dbo.Picture_Category",
                c => new
                    {
                        PictureID = c.Int(nullable: false),
                        CategoryID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PictureID, t.CategoryID })
                .ForeignKey("dbo.Pictures", t => t.PictureID, cascadeDelete: true)
                .ForeignKey("dbo.Categories", t => t.CategoryID, cascadeDelete: true)
                .Index(t => t.PictureID)
                .Index(t => t.CategoryID);
            
            CreateTable(
                "dbo.Picture_Resolution",
                c => new
                    {
                        PictureID = c.Int(nullable: false),
                        ResolutionID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PictureID, t.ResolutionID })
                .ForeignKey("dbo.Pictures", t => t.PictureID, cascadeDelete: true)
                .ForeignKey("dbo.Resolutions", t => t.ResolutionID, cascadeDelete: true)
                .Index(t => t.PictureID)
                .Index(t => t.ResolutionID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Picture_Resolution", new[] { "ResolutionID" });
            DropIndex("dbo.Picture_Resolution", new[] { "PictureID" });
            DropIndex("dbo.Picture_Category", new[] { "CategoryID" });
            DropIndex("dbo.Picture_Category", new[] { "PictureID" });
            DropIndex("dbo.Picture_Tag", new[] { "TagID" });
            DropIndex("dbo.Picture_Tag", new[] { "PictureID" });
            DropIndex("dbo.User_Archievement", new[] { "ArchievementID" });
            DropIndex("dbo.User_Archievement", new[] { "UserID" });
            DropIndex("dbo.Rates", new[] { "PictureID" });
            DropIndex("dbo.Resolutions", new[] { "ResolutionGroupID" });
            DropIndex("dbo.Categories", new[] { "CategoryGroupID" });
            DropIndex("dbo.Comments", new[] { "PictureID" });
            DropIndex("dbo.Pictures", new[] { "UserID" });
            DropIndex("dbo.Archievements", new[] { "ArchievementGroupID" });
            DropForeignKey("dbo.Picture_Resolution", "ResolutionID", "dbo.Resolutions");
            DropForeignKey("dbo.Picture_Resolution", "PictureID", "dbo.Pictures");
            DropForeignKey("dbo.Picture_Category", "CategoryID", "dbo.Categories");
            DropForeignKey("dbo.Picture_Category", "PictureID", "dbo.Pictures");
            DropForeignKey("dbo.Picture_Tag", "TagID", "dbo.Tags");
            DropForeignKey("dbo.Picture_Tag", "PictureID", "dbo.Pictures");
            DropForeignKey("dbo.User_Archievement", "ArchievementID", "dbo.Archievements");
            DropForeignKey("dbo.User_Archievement", "UserID", "dbo.Users");
            DropForeignKey("dbo.Rates", "PictureID", "dbo.Pictures");
            DropForeignKey("dbo.Resolutions", "ResolutionGroupID", "dbo.ResolutionGroups");
            DropForeignKey("dbo.Categories", "CategoryGroupID", "dbo.CategoryGroups");
            DropForeignKey("dbo.Comments", "PictureID", "dbo.Pictures");
            DropForeignKey("dbo.Pictures", "UserID", "dbo.Users");
            DropForeignKey("dbo.Archievements", "ArchievementGroupID", "dbo.ArchievementGroups");
            DropTable("dbo.Picture_Resolution");
            DropTable("dbo.Picture_Category");
            DropTable("dbo.Picture_Tag");
            DropTable("dbo.User_Archievement");
            DropTable("dbo.Rates");
            DropTable("dbo.ResolutionGroups");
            DropTable("dbo.Resolutions");
            DropTable("dbo.CategoryGroups");
            DropTable("dbo.Categories");
            DropTable("dbo.Tags");
            DropTable("dbo.Comments");
            DropTable("dbo.Pictures");
            DropTable("dbo.ArchievementGroups");
            DropTable("dbo.Archievements");
            DropTable("dbo.Users");
        }
    }
}
