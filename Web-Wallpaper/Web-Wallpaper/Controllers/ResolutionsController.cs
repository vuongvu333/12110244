﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_Wallpaper.Models;

namespace Web_Wallpaper.Controllers
{
    public class ResolutionsController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /Resolutions/

        public ActionResult Index()
        {
            var resolutions = db.Resolutions.Include(r => r.ResolutionGroup);
            return View(resolutions.ToList());
        }

        //
        // GET: /Resolutions/Details/5

        public ActionResult Details(int id = 0)
        {
            Resolution resolution = db.Resolutions.Find(id);
            if (resolution == null)
            {
                return HttpNotFound();
            }
            return View(resolution);
        }

        //
        // GET: /Resolutions/Create

        public ActionResult Create()
        {
            ViewBag.ResolutionGroupID = new SelectList(db.ResolutionGroups, "ID", "ResolutionGroupName");
            return View();
        }

        //
        // POST: /Resolutions/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Resolution resolution)
        {
            if (ModelState.IsValid)
            {
                db.Resolutions.Add(resolution);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ResolutionGroupID = new SelectList(db.ResolutionGroups, "ID", "ResolutionGroupName", resolution.ResolutionGroupID);
            return View(resolution);
        }

        //
        // GET: /Resolutions/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Resolution resolution = db.Resolutions.Find(id);
            if (resolution == null)
            {
                return HttpNotFound();
            }
            ViewBag.ResolutionGroupID = new SelectList(db.ResolutionGroups, "ID", "ResolutionGroupName", resolution.ResolutionGroupID);
            return View(resolution);
        }

        //
        // POST: /Resolutions/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Resolution resolution)
        {
            if (ModelState.IsValid)
            {
                db.Entry(resolution).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ResolutionGroupID = new SelectList(db.ResolutionGroups, "ID", "ResolutionGroupName", resolution.ResolutionGroupID);
            return View(resolution);
        }

        //
        // GET: /Resolutions/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Resolution resolution = db.Resolutions.Find(id);
            if (resolution == null)
            {
                return HttpNotFound();
            }
            return View(resolution);
        }

        //
        // POST: /Resolutions/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Resolution resolution = db.Resolutions.Find(id);
            db.Resolutions.Remove(resolution);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}