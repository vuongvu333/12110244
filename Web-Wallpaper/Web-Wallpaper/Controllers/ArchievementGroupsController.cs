﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_Wallpaper.Models;

namespace Web_Wallpaper.Controllers
{
    public class ArchievementGroupsController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /ArchievementGroups/

        public ActionResult Index()
        {
            return View(db.ArchievementGroups.ToList());
        }

        //
        // GET: /ArchievementGroups/Details/5

        public ActionResult Details(int id = 0)
        {
            ArchievementGroup archievementgroup = db.ArchievementGroups.Find(id);
            if (archievementgroup == null)
            {
                return HttpNotFound();
            }
            return View(archievementgroup);
        }

        //
        // GET: /ArchievementGroups/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /ArchievementGroups/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ArchievementGroup archievementgroup)
        {
            if (ModelState.IsValid)
            {
                db.ArchievementGroups.Add(archievementgroup);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(archievementgroup);
        }

        //
        // GET: /ArchievementGroups/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ArchievementGroup archievementgroup = db.ArchievementGroups.Find(id);
            if (archievementgroup == null)
            {
                return HttpNotFound();
            }
            return View(archievementgroup);
        }

        //
        // POST: /ArchievementGroups/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ArchievementGroup archievementgroup)
        {
            if (ModelState.IsValid)
            {
                db.Entry(archievementgroup).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(archievementgroup);
        }

        //
        // GET: /ArchievementGroups/Delete/5

        public ActionResult Delete(int id = 0)
        {
            ArchievementGroup archievementgroup = db.ArchievementGroups.Find(id);
            if (archievementgroup == null)
            {
                return HttpNotFound();
            }
            return View(archievementgroup);
        }

        //
        // POST: /ArchievementGroups/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ArchievementGroup archievementgroup = db.ArchievementGroups.Find(id);
            db.ArchievementGroups.Remove(archievementgroup);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}