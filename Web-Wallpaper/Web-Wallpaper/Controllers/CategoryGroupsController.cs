﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_Wallpaper.Models;

namespace Web_Wallpaper.Controllers
{
    public class CategoryGroupsController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /CategoryGroups/

        public ActionResult Index()
        {
            return View(db.CategoryGroups.ToList());
        }

        //
        // GET: /CategoryGroups/Details/5

        public ActionResult Details(int id = 0)
        {
            CategoryGroup categorygroup = db.CategoryGroups.Find(id);
            if (categorygroup == null)
            {
                return HttpNotFound();
            }
            return View(categorygroup);
        }

        //
        // GET: /CategoryGroups/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /CategoryGroups/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CategoryGroup categorygroup)
        {
            if (ModelState.IsValid)
            {
                db.CategoryGroups.Add(categorygroup);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(categorygroup);
        }

        //
        // GET: /CategoryGroups/Edit/5

        public ActionResult Edit(int id = 0)
        {
            CategoryGroup categorygroup = db.CategoryGroups.Find(id);
            if (categorygroup == null)
            {
                return HttpNotFound();
            }
            return View(categorygroup);
        }

        //
        // POST: /CategoryGroups/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CategoryGroup categorygroup)
        {
            if (ModelState.IsValid)
            {
                db.Entry(categorygroup).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(categorygroup);
        }

        //
        // GET: /CategoryGroups/Delete/5

        public ActionResult Delete(int id = 0)
        {
            CategoryGroup categorygroup = db.CategoryGroups.Find(id);
            if (categorygroup == null)
            {
                return HttpNotFound();
            }
            return View(categorygroup);
        }

        //
        // POST: /CategoryGroups/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CategoryGroup categorygroup = db.CategoryGroups.Find(id);
            db.CategoryGroups.Remove(categorygroup);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}