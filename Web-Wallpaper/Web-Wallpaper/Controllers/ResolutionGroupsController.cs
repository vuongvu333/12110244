﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_Wallpaper.Models;

namespace Web_Wallpaper.Controllers
{
    public class ResolutionGroupsController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /ResolutionGroups/

        public ActionResult Index()
        {
            return View(db.ResolutionGroups.ToList());
        }

        //
        // GET: /ResolutionGroups/Details/5

        public ActionResult Details(int id = 0)
        {
            ResolutionGroup resolutiongroup = db.ResolutionGroups.Find(id);
            if (resolutiongroup == null)
            {
                return HttpNotFound();
            }
            return View(resolutiongroup);
        }

        //
        // GET: /ResolutionGroups/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /ResolutionGroups/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ResolutionGroup resolutiongroup)
        {
            if (ModelState.IsValid)
            {
                db.ResolutionGroups.Add(resolutiongroup);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(resolutiongroup);
        }

        //
        // GET: /ResolutionGroups/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ResolutionGroup resolutiongroup = db.ResolutionGroups.Find(id);
            if (resolutiongroup == null)
            {
                return HttpNotFound();
            }
            return View(resolutiongroup);
        }

        //
        // POST: /ResolutionGroups/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ResolutionGroup resolutiongroup)
        {
            if (ModelState.IsValid)
            {
                db.Entry(resolutiongroup).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(resolutiongroup);
        }

        //
        // GET: /ResolutionGroups/Delete/5

        public ActionResult Delete(int id = 0)
        {
            ResolutionGroup resolutiongroup = db.ResolutionGroups.Find(id);
            if (resolutiongroup == null)
            {
                return HttpNotFound();
            }
            return View(resolutiongroup);
        }

        //
        // POST: /ResolutionGroups/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ResolutionGroup resolutiongroup = db.ResolutionGroups.Find(id);
            db.ResolutionGroups.Remove(resolutiongroup);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}