﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_Wallpaper.Models;

namespace Web_Wallpaper.Controllers
{
    public class RatesController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /Rates/

        public ActionResult Index()
        {
            var rates = db.Rates.Include(r => r.Picture);
            return View(rates.ToList());
        }

        //
        // GET: /Rates/Details/5

        public ActionResult Details(int id = 0)
        {
            Rate rate = db.Rates.Find(id);
            if (rate == null)
            {
                return HttpNotFound();
            }
            return View(rate);
        }

        //
        // GET: /Rates/Create

        public ActionResult Create()
        {
            ViewBag.PictureID = new SelectList(db.Pictures, "PictureID", "PictureName");
            return View();
        }

        //
        // POST: /Rates/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Rate rate)
        {
            if (ModelState.IsValid)
            {
                db.Rates.Add(rate);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PictureID = new SelectList(db.Pictures, "PictureID", "PictureName", rate.PictureID);
            return View(rate);
        }

        //
        // GET: /Rates/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Rate rate = db.Rates.Find(id);
            if (rate == null)
            {
                return HttpNotFound();
            }
            ViewBag.PictureID = new SelectList(db.Pictures, "PictureID", "PictureName", rate.PictureID);
            return View(rate);
        }

        //
        // POST: /Rates/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Rate rate)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rate).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PictureID = new SelectList(db.Pictures, "PictureID", "PictureName", rate.PictureID);
            return View(rate);
        }

        //
        // GET: /Rates/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Rate rate = db.Rates.Find(id);
            if (rate == null)
            {
                return HttpNotFound();
            }
            return View(rate);
        }

        //
        // POST: /Rates/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Rate rate = db.Rates.Find(id);
            db.Rates.Remove(rate);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}