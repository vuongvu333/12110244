﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_Wallpaper.Models;

namespace Web_Wallpaper.Controllers
{
    public class ArchievementsController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /Archievements/

        public ActionResult Index()
        {
            var archievements = db.Archievements.Include(a => a.ArchievementGroup);
            return View(archievements.ToList());
        }

        //
        // GET: /Archievements/Details/5

        public ActionResult Details(int id = 0)
        {
            Archievement archievement = db.Archievements.Find(id);
            if (archievement == null)
            {
                return HttpNotFound();
            }
            return View(archievement);
        }

        //
        // GET: /Archievements/Create

        public ActionResult Create()
        {
            ViewBag.ArchievementGroupID = new SelectList(db.ArchievementGroups, "ID", "ArchievementGroupName");
            return View();
        }

        //
        // POST: /Archievements/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Archievement archievement)
        {
            if (ModelState.IsValid)
            {
                db.Archievements.Add(archievement);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ArchievementGroupID = new SelectList(db.ArchievementGroups, "ID", "ArchievementGroupName", archievement.ArchievementGroupID);
            return View(archievement);
        }

        //
        // GET: /Archievements/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Archievement archievement = db.Archievements.Find(id);
            if (archievement == null)
            {
                return HttpNotFound();
            }
            ViewBag.ArchievementGroupID = new SelectList(db.ArchievementGroups, "ID", "ArchievementGroupName", archievement.ArchievementGroupID);
            return View(archievement);
        }

        //
        // POST: /Archievements/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Archievement archievement)
        {
            if (ModelState.IsValid)
            {
                db.Entry(archievement).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ArchievementGroupID = new SelectList(db.ArchievementGroups, "ID", "ArchievementGroupName", archievement.ArchievementGroupID);
            return View(archievement);
        }

        //
        // GET: /Archievements/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Archievement archievement = db.Archievements.Find(id);
            if (archievement == null)
            {
                return HttpNotFound();
            }
            return View(archievement);
        }

        //
        // POST: /Archievements/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Archievement archievement = db.Archievements.Find(id);
            db.Archievements.Remove(archievement);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}