﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Comment
    {
        //[Range(1, 100)]
        public int ID { get; set; }
        [Required(ErrorMessage = "Nội dung không được bỏ trống!")]
        [StringLength(int.MaxValue, ErrorMessage = "Nội dung phải có ít nhất 50 ký tự!", MinimumLength = 50)]
        public String Body { get; set; }
        [Required(ErrorMessage = "Nội dung không được bỏ trống!")]
        [DataType(DataType.DateTime, ErrorMessage = "Nhập đúng kiểu dữ liệu!")]
        public DateTime DateCreated { get; set; }
        [Required(ErrorMessage = "Nội dung không được bỏ trống!")]
        [DataType(DataType.DateTime, ErrorMessage = "Nhập đúng kiểu dữ liệu!")]
        public DateTime DateUpdated { get; set; }
        [Required(ErrorMessage = "Nội dung không được bỏ trống!")]
        public String Author { get; set; }

        public int PostID { get; set; }
        public virtual Post Posts { get; set; }
    }
}