﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Tag
    {
        //[Range(1, 100)]
        public int ID { get; set; }
        [Required(ErrorMessage = "Nội dung không được bỏ trống.")]
        [StringLength(100, ErrorMessage = "Độ dài nội dung phải không ít hơn 10 và lớn hơn 100 ký tự!!!", MinimumLength = 10)]
        public String Content { get; set; }

        public virtual ICollection<Post> Posts { get; set; }
    }
}