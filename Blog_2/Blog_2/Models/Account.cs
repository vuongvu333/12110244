﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Account
    {
        //[Range(1, 100)]
        public int ID { get; set; }
        [Required(ErrorMessage = "Nội dung không được bỏ trống!")]
        [StringLength(int.MaxValue, ErrorMessage = "Mật khẩu phải có ít nhất 6 ký tự!", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public String Password { get; set; }
        [Required(ErrorMessage = "Nội dung không được bỏ trống")]
        [EmailAddress(ErrorMessage = "Nhập đúng kiểu email")]
        public String Email { get; set; }
        [Required(ErrorMessage = "Nội dung không được bỏ trống")]
        [StringLength(100, ErrorMessage = "Nội dung có tối đa 100 ký tự.")]
        public String FirstName { get; set; }
        [Required(ErrorMessage = "Nội dung không được bỏ trống")]
        [StringLength(100, ErrorMessage = "Nội dung có tối đa 100 ký tự.")]
        public String LastName { get; set; }

        public virtual ICollection<Post> Posts { get; set; }
    }
}