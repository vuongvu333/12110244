﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog_3.Models;

namespace Blog_3.Controllers
{
    [Authorize]
    public class PostController : Controller
    {
        private BlogContext db = new BlogContext();

        //
        // GET: /Post/
        [AllowAnonymous]
        public ActionResult Index()
        {
            var posts = db.Posts.Include(p => p.UserProfile);
            return View(posts.ToList());
        }

        //
        // GET: /Post/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            Post post = db.Posts.Find(id);
            ViewData["idpost"] = id;
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        public ActionResult PostLinkTag(int id=0)
        {
            Tag tag = db.Tags.Find(id);
            return View(tag);
        }

        //
        // GET: /Post/Create

        public ActionResult Create()
        {
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }
        
        //
        // POST: /Post/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Post post, String Content)
        {
            if (ModelState.IsValid)
            {
                post.DateCreated = DateTime.Now;
                post.DateUpdated = DateTime.Now;
                post.UserProfileUserID = db.UserProfiles.Select(s => new { s.UserId, s.UserName })
                    .Where(d => d.UserName == User.Identity.Name).Single().UserId;
                
                // tao list cac tag
                List<Tag> Tags = new List<Tag>();
                // tach cac tag theo dau,
                string[] TagContent = Content.Split(',');
                // lap cac tag vua tach
                foreach (string item in TagContent)
                {
                    // tim xem tag comtem da co hay chua
                    Tag tagExits = null;
                    var listTag = db.Tags.Where(y => y.Content.Equals(item));
                    if (listTag.Count() > 0)
                    {
                        // neu co tag roi thi add them post vao
                        tagExits = listTag.First();
                        tagExits.Posts.Add(post);

                    }
                    else
                    {
                        // neu chua thi tao tag moi
                        tagExits = new Tag();
                        tagExits.Content = item;
                        tagExits.Posts = new List<Post>();
                        tagExits.Posts.Add(post);
                    }
                    //add vao list cac tag
                    Tags.Add(tagExits);

                }
                // gan lis tag cho post
                post.Tags = Tags;
                db.Posts.Add(post);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserProfileUserID);
            return View(post);
        }

        //
        // GET: /Post/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserProfileUserID);
            return View(post);
        }

        //
        // POST: /Post/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Post post)
        {
            if (ModelState.IsValid)
            {
                post.DateUpdated = DateTime.Now;
                var date = (from n in db.Posts
                            where n.ID == post.ID
                            select n.DateCreated).Single();

                post.DateCreated = date;
                post.UserProfileUserID = db.UserProfiles.Select(a => new { a.UserId, a.UserName })
                    .Where(s => s.UserName == User.Identity.Name).Single().UserId;
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserProfileUserID);
            return View(post);
        }

        //
        // GET: /Post/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Post/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}