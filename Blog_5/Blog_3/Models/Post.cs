﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog_3.Models
{
    public class Post
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Required(ErrorMessage = "Nội dung không được bỏ trống")]
        [StringLength(500, ErrorMessage = "Độ dài ký tự phải từ 20 đến 500!!!", MinimumLength = 20)]
        public String Title { get; set; }
        [Required(ErrorMessage = "Nội dung không được bỏ trống")]
        [StringLength(int.MaxValue, ErrorMessage = "Độ dài ký tự phải ít nhất là 50 từ.", MinimumLength = 50)]
        public String Body { get; set; }
        [Required(ErrorMessage = "Nội dung không được bỏ trống")]
        [DataType(DataType.DateTime, ErrorMessage = "Nhập đúng kiểu dữ liệu!")]
        public DateTime DateCreated { get; set; }
        [Required(ErrorMessage = "Nội dung không được bỏ trống!")]
        [DataType(DataType.DateTime, ErrorMessage = "Nhập đúng kiểu dữ liệu!")]
        public DateTime DateUpdated { get; set; }

        public int UserProfileUserID { get; set; }
        public virtual UserProfile UserProfile { get; set; } 
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
    }
}