﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class Comment
    {
        [Key]
        public int ID { get; set; }
        public int PostID { get; set; }
        public string Body { get; set; }
    }
}